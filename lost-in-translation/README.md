## Available Scripts

In the project directory, you can run:

### `npm i`

Installs all dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

## Structure

### Easier imports

Imports is made easier with an index.js in each component folder. This exports the component by default.