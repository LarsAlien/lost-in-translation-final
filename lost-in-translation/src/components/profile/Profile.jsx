import styles from "./Profile.module.css";
import { useHistory } from "react-router-dom";
import mankey from "../../assets/mankey.png";
import { useDispatch, useSelector } from "react-redux";
import { profileLogoutAction } from "../../store/actions/profileActions";
import { useEffect } from "react"
import { ProfileTranslationsHistory } from './ProfileTranslationsHistory'
import { translateDeleteHistoryAction } from '../../store/actions/translateActions'
import { getStorage } from '../../utils/storage'


const Profile = (props) => {
  const dispatch = useDispatch();
  const { username, translations } = useSelector((state) => state.profileReducer);
  const history = useHistory()

  useEffect(() => {
    if(!getStorage('_lit-profile') || !username) {
      history.push(`/`)
    }
  }, [])

  const handleLogoutClick = () => {
    dispatch(profileLogoutAction())
    history.push('/')
  }
  const handleTranslateMoreClick = () => {
    history.push('/translate/')
  }

  const handleDeleteHistoryClick = () => {
    dispatch(translateDeleteHistoryAction(username))
  }

  return (
    <div className={styles.Profile}>
      <div className={styles.UpperDiv}>
      <img src={mankey} alt="" className={styles.Logo}/>

      <h1>{username}`s profile</h1>
      <div className={styles.ButtonContainer}>
        <button onClick={handleLogoutClick} className={styles.ProfileButton}>Log out</button>
        <button onClick={handleTranslateMoreClick} className={styles.ProfileButton}>Translate more</button>
        <button onClick={handleDeleteHistoryClick} className={styles.ProfileButton}>Delete history</button>
      </div>
      </div>

      <ProfileTranslationsHistory />
    </div>
  );
};

export default Profile;
