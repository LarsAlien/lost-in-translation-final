import styles from './ProfileTranslationsHistory.module.css';
import { useSelector } from "react-redux";


export const ProfileTranslationsHistory = () => {
    const { translations } = useSelector((state) => state.profileReducer);
    
    return (
        <div className={styles.TranslateOutput}>
            <h3 className={styles.OutputTitle}>Translation history:</h3>
            <ul className={styles.OutputList}>
                {
                    translations.map((translation, i) => (
                        <li key={i} className={styles.OutputListItem}>{ translation }</li>
                    ))
                }
            </ul>
        </div>
      );
}