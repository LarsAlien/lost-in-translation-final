import styles from "./TranslateInput.module.css";
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react'
import { translateAction } from '../../store/actions/translateActions'

const TranslateInput = () => {

  const dispatch = useDispatch()
  const { username } = useSelector((state) => state.profileReducer)
  const [translationInput, setTranslation] = useState('')

  const handleTranslationChanged = (event) => {
    const regex = /[^A-Za-z" "]/g
    setTranslation(event.target.value.replace(regex, ''))
  }

  const onTranslateClick = (e) => {
    e.preventDefault()
    if (translationInput === '') return;
    dispatch(translateAction(
      {
        translation: translationInput.trim(),
        username: username
      }
    ))
  };

  return (
    <form className={styles.Form}>
      <input
        type="text"
        placeholder="Enter what you want to translate..."
        className={styles.Input}
        maxLength="20"
        value={translationInput}
        onChange={handleTranslationChanged}
      />
      <button
        type="text"
        className={styles.SubmitButton}
        onClick={onTranslateClick}
      >Translate</button>
    </form>
  );
};

export default TranslateInput;
