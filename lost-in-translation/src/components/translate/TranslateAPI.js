const BASE_URL = 'http://localhost:5000'

export function patchTranslations( payload, id ) {
    const data = {
        translations: payload
    }
    return fetch(`${BASE_URL}/users/${id}`, { 
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(r => r.json())
}