import { useSelector } from "react-redux";
import styles from "./TranslateOutput.module.css";

const TranslateOutput = (props) => {
  const { error, translation } = useSelector((state) => state.translateReducer);

  let splitTranslation = translation.split("")

  const signs = (array) => {
    array = array.map((c) => (c = c.toLowerCase()));
    return array.map((char) => {
      if (char === " ") char = '_'
      return (
        <div className={styles.OutputSignContainer}>
          <img
            src={require(`../../assets/individial_signs/${char}.png`).default}
            alt={char}
            className={styles.OutputSign}
          />
        </div>
      );
    });
  };

  return (
    <div className={styles.TranslateOutput}>
      <h3 className={styles.OutputTitle}>Your translation:</h3>
      <h4 className={styles.OutputTranslation}>{translation}</h4>
      <div className={styles.OutputContainer}>{signs(splitTranslation)}</div>
    </div>
  );
};

export default TranslateOutput;
