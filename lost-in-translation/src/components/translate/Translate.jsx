import TranslateInput from "./TranslateInput";
import TranslateOutput from "./TranslateOutput";
import { useEffect } from "react"
import { useHistory } from "react-router-dom";
import { getStorage } from '../../utils/storage'




const Translate = () => {
  const history = useHistory()

  useEffect(() => {
    if(!getStorage('_lit-profile')) {
      history.push(`/`)
    }
  }, [])
  return (
    <>
      <TranslateInput />
      <TranslateOutput />
    </>
  );
};

export default Translate;
