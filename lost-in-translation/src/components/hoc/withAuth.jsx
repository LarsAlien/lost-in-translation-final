import { Redirect } from "react-router-dom";

const withAuth = (Component) => (props) => {

  if(localStorage.getItem('_lit-profile'))  {
    console.log('TOCOMPONENT');
    return <Component {...props} />;
  } else {
    console.log('TOHOME');

    return <Redirect to="/" />;
  }
};

export default withAuth;
