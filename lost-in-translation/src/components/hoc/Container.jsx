import styles from "./Container.module.css";

const Container = props => {
    return (
        <main className={styles.Container}>
            { props.children }
        </main>
    )
}

export default Container
