import styles from "./Header.module.css";
import litLogo from "../../assets/litLogo.png";
import mankey from "../../assets/mankey.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserTie } from "@fortawesome/free-solid-svg-icons";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Header = () => {
  const { username } = useSelector((state) => state.profileReducer);

  const history = useHistory();

  const onProfileClick = () => {
    history.push(`/profile/${username}`);
  };

  const onLitLogoClick = () => {
    history.push(`/`);
  };

  return (
    <header className={styles.Header}>
      <div onClick={onLitLogoClick} className={styles.HeaderBrand}>
        <img src={mankey} className={styles.HeaderLogoMankey} alt="logo" />
        <img src={litLogo} className={styles.HeaderLogo} alt="logo" />
      </div>

      {localStorage.getItem('_lit-profile') && (
        <div className={styles.HeaderProfile} onClick={onProfileClick}>
          <label htmlFor="userTieIcon" className={styles.HeaderUsername}>
            {username}
          </label>
          <FontAwesomeIcon
            id="userTieIcon"
            icon={faUserTie}
            size="2x"
          />
        </div>
      )}
    </header>
  );
};

export default Header;
