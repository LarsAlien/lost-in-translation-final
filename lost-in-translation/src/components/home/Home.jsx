import styles from "./Home.module.css";
import Input from './HomeInput'

const Home = () => {
    return(
        <div className={styles.Home}>
            <Input />
        </div>
    )
}

export default Home