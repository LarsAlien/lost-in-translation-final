import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useEffect, useState } from 'react'
import { getStorage } from '../../utils/storage'

import styles from './HomeInput.module.css';

import { 
  loginAction
} from '../../store/actions/loginActions'

const HomeInput = props => {
  const history = useHistory() 

  const dispatch = useDispatch()
  const {error, fetching} = useSelector(state => state.loginReducer)
  const {username} = useSelector(state => state.profileReducer)
  const [usernameInput, setUsername] = useState('')

  const onUsernameChanged = (event) => {
    setUsername(event.target.value);
  };

  useEffect(() => { //if localstorage is set,  populating state with current localstorage and redirecting to next page
      if(getStorage('_lit-profile')) {
      const lsUser = JSON.parse(getStorage('_lit-profile'))
      dispatch(loginAction(lsUser.username))
      history.push(`/translate/`)
    }
  }, [username])

  const onLoginClick = () => {
    if (usernameInput === '') return;
    dispatch(loginAction(usernameInput.trim()))
    history.push(`/translate/`)
  };

  return (
    <section className={styles.Form}>
      {fetching &&
      <p>Fetching user....</p>}
      {error &&
      <p>Error: {error}</p>}
      <input
        type='text'
        placeholder='Enter username...'
        className={styles.Input}
        maxLength='25'
        value={usernameInput}
        onChange={onUsernameChanged}
      />
      <button
        type='text'
        className={styles.SubmitButton}
        onClick={onLoginClick}
      >
        Log in
        
      </button>
    </section>
  );
};

export default HomeInput;
