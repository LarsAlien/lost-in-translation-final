const BASE_URL = 'http://localhost:5000'

export function fetchUsernames() {
    return fetch(`${BASE_URL}/users`)
        .then(r => r.json())
        .then( users => {
            console.log(users);
        })
}

export function fetchUser( username ) {
    return fetch(`${BASE_URL}/users`)
        .then(r => r.json())
        .then( users => {
            return users.find(u => u.username === username)
        })
}

export function fetchUserById( id ) {
    return fetch(`${BASE_URL}/users`)
        .then(r => r.json())
        .then( users => {
            return users.find(u => u.id === id)
        })
}

export function createUsername( payload ) {
    return fetch(`${BASE_URL}/users`, { 
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username: payload.username,
            translations: payload.translations 
        })
    })
    .then(r => r.json()).then()//only 10 translations (pagination) slice, 
}