import { createContext, useReducer } from 'react'

export const AppContext = createContext();
export const ACTION_SET_USERNAME = 'username:SET'

function appReducer(state, action) {
    switch ( action.type ) {
        case 'username:SET':
            return {
                username: action.payload
            }
        default:
            return state
    }
}

const initialState = {
    username: ''
}

export default function AppProvider( props ) {

    const [ state, dispatch ] = useReducer(appReducer, initialState)

    return (
        <AppContext.Provider value={ [ state, dispatch ] }>
            { props.children }
        </AppContext.Provider>
    )
}