import {
    ACTION_PROFILE_LOGOUT,
    ACTION_PROFILE_SET,
  } from "../actions/profileActions";
import { translateLogoutAction } from "../actions/translateActions"

import { setStorage, removeStorage } from '../../utils/storage'
  
  
const profileStorageKey = '_lit-profile' 

  export const profileMiddleware = ({ dispatch }) => (next) => (action) => {
    

  
    if (action.type === ACTION_PROFILE_SET) {
      setStorage(profileStorageKey, JSON.stringify(action.payload))
    }

    if(action.type === ACTION_PROFILE_LOGOUT) {
        removeStorage(profileStorageKey)
        dispatch(translateLogoutAction())
    }
  
   

    return next(action);
  };
  