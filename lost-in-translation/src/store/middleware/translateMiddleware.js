import {
  ACTION_TRANSLATE_PATCH,
  ACTION_TRANSLATE,
  ACTION_TRANSLATE_DELETE
} from "../actions/translateActions";
import {
  translateErrorAction,
  translatePatchAction,
} from "../actions/translateActions";
import { profileSetAction } from "../actions/profileActions";
import { patchTranslations } from "../../components/translate/TranslateAPI";
import { fetchUser } from "../../components/home/HomeAPI";

export const translateMiddleware = ({ dispatch }) => (next) => async (action) => {
  next(action);

  if (action.type === ACTION_TRANSLATE) {
    dispatch(translatePatchAction(action.payload));
  }

  if (action.type === ACTION_TRANSLATE_DELETE) {
    try {
      await fetchUser(action.payload)
        .then(async (r) => {
          await patchTranslations(
            [],
            r.id
          ).then(res => {
            dispatch(profileSetAction(res))
          })
        })
      
    } catch (err) {
      console.log(err.message);
      dispatch(translateErrorAction(err.message));
    }
  }

  if (action.type === ACTION_TRANSLATE_PATCH) {
    try {
      await fetchUser(action.payload.username)
        .then(async (r) => {
          await patchTranslations(
            [...r.translations, action.payload.translation],
            r.id
          ).then(res => {
            // const updatedUser = await fetchUserById(action.payload.id);
            dispatch(profileSetAction(res))
          })
        })
      
    } catch (err) {
      console.log(err.message);
      dispatch(translateErrorAction(err.message));
    }
  }
};
