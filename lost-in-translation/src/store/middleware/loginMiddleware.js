import {
  ACTION_LOGIN_AUTHENTIICATE,
  ACTION_LOGIN,
  ACTION_LOGIN_CREATE,
} from "../actions/loginActions"
import { fetchUser, createUsername} from "../../components/home/HomeAPI"
import {
  loginErrorAction,
  loginAuthenticateAction,
  loginCreateAction,
} from "../actions/loginActions"
import { profileSetAction } from '../actions/profileActions'


export const loginMiddleware = ({ dispatch }) => (next) => async (action) => {
  next(action)

  if (action.type === ACTION_LOGIN_CREATE) {
    try {
      const newUser = await createUsername({
        username: action.payload,
        translations: []
        });
      dispatch(profileSetAction(newUser))
    } catch (err) {
      console.log(err.message)
      dispatch(loginErrorAction('Cant create user: '+err.message))
    }
  }

  if (action.type === ACTION_LOGIN) {
    dispatch(loginAuthenticateAction(action.payload))
  }

  if (action.type === ACTION_LOGIN_AUTHENTIICATE) {
    try {
      const existingUser = await fetchUser(action.payload)
      if (existingUser) {
        //Return 10 translations
        let user = {
            ...existingUser,
            translations: existingUser.translations.slice(existingUser.translations.length,-10)
          }
           
        dispatch(profileSetAction(user))
      } else {
        dispatch(loginCreateAction(action.payload))
      }
    
    } catch (error) {
        dispatch(loginErrorAction('Cant authenticate user: '+error.message))
    }
  }
}
