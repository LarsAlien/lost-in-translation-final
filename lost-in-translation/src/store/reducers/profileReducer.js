import { 
    ACTION_PROFILE_SET, 
    ACTION_PROFILE_ERROR,
    ACTION_PROFILE_LOGOUT
} from '../actions/profileActions'

const initialState = {
    username: null,
    translations: [],
    error: null

}

export function profileReducer(state = initialState, action) {

    switch( action.type ) {
        
        case ACTION_PROFILE_SET:
            return {
                ...state,
                ...action.payload
            }

        case ACTION_PROFILE_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case ACTION_PROFILE_LOGOUT:
            return initialState

        default:
            return state
    }

}