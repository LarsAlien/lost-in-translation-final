import { combineReducers } from "redux";
import { loginReducer } from './loginReducer'
import { profileReducer } from './profileReducer'
import { translateReducer } from './translateReducer'

export const rootReducer = combineReducers({
    loginReducer,
    profileReducer,
    translateReducer
})