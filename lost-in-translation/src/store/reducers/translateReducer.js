import {
    ACTION_TRANSLATE_ERROR,
    ACTION_TRANSLATE_PATCH,
    ACTION_TRANSLATE,
    ACTION_TRANSLATE_LOGOUT,
    ACTION_TRANSLATE_DELETE
  } from "../actions/translateActions"

const initialState = {
    translation: '',
    error: null,
    patching: false

}

export function translateReducer(state = initialState, action) {

    switch( action.type ) {

        case ACTION_TRANSLATE:
            return {
                ...state,
                patching: true,
                translation: action.payload.translation
            }
        
        case ACTION_TRANSLATE_PATCH:
            return {
                ...state,
                error: null,
                patching: false
            }

        case ACTION_TRANSLATE_DELETE:
            return {
                ...state,
                error: null,
                patching: false,
                translation: ''
            }

        case ACTION_TRANSLATE_ERROR:
            return {
                ...state,
                error: action.payload
            }

        case ACTION_TRANSLATE_LOGOUT:
        return initialState

        default:
            return state
    }

}