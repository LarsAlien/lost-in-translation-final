import { 
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN,
    ACTION_LOGIN_AUTHENTIICATE
} from '../actions/loginActions'

const initialState = {
    username: '',
    fetching: false,
    error: null
}

export function loginReducer(state = initialState, action) {

    switch( action.type ) {

        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            }
        case ACTION_LOGIN:
            return {
                ...state,
                username: action.payload,
                fetching: true
                        }
        
        case ACTION_LOGIN_AUTHENTIICATE:
            return {
                ...state,
                username: action.payload,
                fetching: false,
                error: null
            }

        default:
            return state
    }

}