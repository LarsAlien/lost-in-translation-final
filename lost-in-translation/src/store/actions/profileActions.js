export const ACTION_PROFILE_SET = 'profile:SET'
export const ACTION_PROFILE_ERROR = 'profile:ERROR'
export const ACTION_PROFILE_LOGOUT = 'profile:LOGOUT'

//fetch profile fro translation



export const profileSetAction = payload => ({
    type: ACTION_PROFILE_SET,
    payload
})

export const profileErrorAction = payload => ({
    type: ACTION_PROFILE_ERROR,
    payload
})

export const profileLogoutAction = () => ({
    type: ACTION_PROFILE_LOGOUT
})

