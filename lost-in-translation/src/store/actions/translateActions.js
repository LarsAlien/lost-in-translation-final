export const ACTION_TRANSLATE_PATCH = 'translate:PATCH'
export const ACTION_TRANSLATE_ERROR = 'translate:ERROR'
export const ACTION_TRANSLATE = 'translate:TRANSLATE'
export const ACTION_TRANSLATE_LOGOUT = 'translate:LOGOUT'
export const ACTION_TRANSLATE_DELETE = 'translate:DELETE'


export const translatePatchAction = payload => ({
    type: ACTION_TRANSLATE_PATCH,
    payload
})

export const translateErrorAction = payload => ({
    type: ACTION_TRANSLATE_ERROR,
    payload
})

export const translateAction = payload => ({
    type: ACTION_TRANSLATE,
    payload
})

export const translateLogoutAction = () => ({
    type: ACTION_TRANSLATE_LOGOUT
})

export const translateDeleteHistoryAction = payload => ({
    type: ACTION_TRANSLATE_DELETE,
    payload
})


