export const ACTION_LOGIN_CREATE = 'login:CREATE'
export const ACTION_LOGIN_ERROR = 'login:ERROR'
export const ACTION_LOGIN = 'login:LOGIN'
export const ACTION_LOGIN_AUTHENTIICATE = 'login:AUTHENTIICATE'

export const loginCreateAction = payload => ({
    type: ACTION_LOGIN_CREATE,
    payload
})

export const loginErrorAction = payload => ({
    type: ACTION_LOGIN_ERROR,
    payload
})

export const loginAction = payload => ({
    type: ACTION_LOGIN,
    payload
})
export const loginAuthenticateAction = payload => ({
    type: ACTION_LOGIN_AUTHENTIICATE,
    payload
})