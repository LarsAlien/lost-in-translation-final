import { applyMiddleware } from 'redux'
import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { rootReducer } from './reducers'
import { loginMiddleware } from './middleware/loginMiddleware'
import { profileMiddleware } from './middleware/profileMiddleware'
import { translateMiddleware } from './middleware/translateMiddleware'


export const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware( loginMiddleware, profileMiddleware, translateMiddleware )
    )
)