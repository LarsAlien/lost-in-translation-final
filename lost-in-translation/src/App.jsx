import "./App.css"
import Header from "./components/header"
import Home from "./components/home"
import Profile from "./components/profile"
import NotFound from "./components/notFound"
import Translate from './components/translate'
import Container from './components/hoc/Container'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Container>
          <Switch>
            <Route exact path="/" component= { Home } />
            <Route path="/translate/" component= { Translate } />
            <Route path="/profile/:id" component= { Profile } />
            <Route path="*" component= { NotFound } />
          </Switch>
        </Container>
      </div>
    </Router>
  )
}

export default App
