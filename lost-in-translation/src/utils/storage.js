export const setStorage = (key, value) => {
const json = JSON.stringify(value)
const hashed =  btoa(json)
localStorage.setItem(key, hashed)
}

export const getStorage = (key) => {
    const storage = localStorage.getItem(key)
    if(!storage) {
        return false
    }

    const unHashed = atob(storage)
    return JSON.parse(unHashed)
}

export const removeStorage = (key) => {
    localStorage.removeItem(key)
}